import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import styles from "./Loginpage.module.css";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import {useHistory} from "react-router-dom";
import Home from "../components/Home/Home";
import {useState} from "react";

const Loginpage = () => {
  const [user, setUser] = useState({
    username: "",
    password: "",
  });
  let history = useHistory();
  const submitForm = (e) => {
    e.preventDefault();
    // const moveToHomepage=()=>{
    //     history.push("/Home");
    // }
    console.log("user", user);
    if (user.username === "shivani" && user.password === "Sinya@123") {
      history.push("/Home");
    } else {
      alert("inValid password");
    }
  };
  const handleUsername = (e) => {
    setUser({...user, username: e.target.value});
  };
  const handlePassword = (e) => {
    setUser({...user, password: e.target.value});
  };

  return (
    <div>
      <Box
        className={styles.loginpage}
        sx={{
          display: "flex",
          flexWrap: "wrap",
          "& > :not(style)": {
            m: 1,
            width: 400,
            height: 400,
          },
        }}
      >
        <Paper elevation={3}>
          <form onSubmit={submitForm}>
            <h2 className={styles.text}>Login Form</h2>
            <div className={styles.username}>
              <TextField
                id="outlined-basic"
                label="username"
                variant="outlined"
                value={user.username}
                onChange={handleUsername}
              />
            </div>

            <div className={styles.password}>
              <TextField
                id="outlined-basic"
                label="password"
                variant="outlined"
                value={user.password}
                onChange={handlePassword}
              />
            </div>
            <div className={styles.loginbtn}>
              <Button type="submit" variant="contained">
                Login
              </Button>
            </div>
          </form>
        </Paper>
      </Box>
    </div>
  );
};
export default Loginpage;
