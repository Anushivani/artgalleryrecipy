import Loginpage from "./Loginpage/Loginpage";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from "./components/Home/Home";

function App() {
  return (
    <div>
      {/* <Loginpage/> */}
      <Router>
        <Switch>
          <Route path="/" component={Loginpage} exact />
          <Route path="/Home" component={Home} exact />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
