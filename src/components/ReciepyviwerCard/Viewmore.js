// import { styled, alpha } from '@mui/material/styles';
// import Button from '@mui/material/Button';
// import Menu from '@mui/material/Menu';
// import MenuItem from '@mui/material/MenuItem';
// import EditIcon from '@mui/icons-material/Edit';
// import Divider from '@mui/material/Divider';
// import ArchiveIcon from '@mui/icons-material/Archive';
// import FileCopyIcon from '@mui/icons-material/FileCopy';
// import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
// import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
//import Button from '@mui/material/Button';
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import {makeStyles} from "@material-ui/core/styles";
import {useState} from "react";
import Overlay from "./Overlay";
import Popup from "./Popup";
const Viewmore = () => {
  // const [pageOpen, setPageOpen] = useState(false);
  // const openMyordersPage = () => {
  //   setPageOpen(true);
  // };
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  return (
    <div>
      <Button sx={{minWidth: 1}}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">More</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            //   value={age}
            label="Age"
            //onChange={handleChange}
          >
            <Button onClick={handleOpen}>
              <MenuItem value="myorders">My Orders</MenuItem>
            </Button>
            <Popup
              handleOpen={handleOpen}
              handleClose={handleClose}
              open={open}
              setOpen={setOpen}
            />

            <div>
              <MenuItem value="addtocart">Add to Cart</MenuItem>
            </div>

            <div>
              <MenuItem value="price">Price</MenuItem>
            </div>
          </Select>
        </FormControl>
      </Button>
    </div>
  );
};
export default Viewmore;
