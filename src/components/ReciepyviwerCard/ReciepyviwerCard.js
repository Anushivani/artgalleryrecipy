import {styled} from "@mui/material/styles";
import React, {useState} from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import {grey, red} from "@mui/material/colors";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
///import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from "@mui/icons-material/MoreVert";
import Img from "../Image/Img";
import {StylesContext} from "@material-ui/styles";
///import styles from "./../ReciepyviwerCard.module.css";
// import Checkbox from '@material-ui/core/Checkbox';
// import Favorite from '@material-ui/icons/Favorite';
// import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
// import { faHeart } from "@fortawesome/free-solid-svg-icons";
// import { faHeartBroken } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Favorite from "@material-ui/icons/Favorite";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
///import ShoppingCart from "@bit/mui-org.material-ui-icons.shopping-cart";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/Inbox";
import DraftsIcon from "@mui/icons-material/Drafts";
//import Menu from "./Menu";
import Viewmore from "./Viewmore";
//import Overlay from "./Overlay";
//import Shoppingcart from "./Shoppingcart";
//import {AddShoppingCart} from "@material-ui/icons";
///import Item from "./Item/Item";
//import Cart from "./Cart/Cart";
//import {DropdownShareButton} from "./Sharebtn";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import green from "@material-ui/core/colors/green";

const ReciepyviwerCard = () => {
  const [expanded, setExpanded] = React.useState(false);
  ///const [like,setLike]=useState(false);
  const [btnColor, setBtnColor] = useState(false);
  const [addtocart,setAddtoCart]=useState({color:"green"});

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const addToCart=()=>{
    
    const val=Object.assign({},{color:addtocart.color==="green"?"red":"green"});
    ///{color:addtocart==="green"?"red":"green"}
    setAddtoCart(val);
   
    // setAddtoCart("unlike"?"grey":"black");
  }
  const recipyLike = () => {
    // setLike(!like);
    // setLike(newColor);
    // style={backgroundColor: `${like}`}
    //setBtnColor("red");
    // btnColor == false ? (<FontAwesomeIcon icon={faHeart} />):(
    //   <FontAwesomeIcon icon={faHeartBroken} />)
    // setBtnColor({style:background-color:red})
    //let likebtn = "grey" ? "notseen" : "red";
    setBtnColor(false ? "grey" : "red");

    console.log(btnColor, "btnColor");
  };
console.log("green");
  return (
    <div>
      <Card sx={({manWidth: 280}, {margin: 2})}>
        <CardHeader
          avatar={
            <Avatar sx={{bgcolor: red[500]}} aria-label="recipe">
              P
            </Avatar>
          }
          action={
            //   <ListItem disablePadding>
            //   <ListItemButton>
            //     <ListItemIcon>
            //       <InboxIcon />
            //     </ListItemIcon>
            //     <ListItemText primary="Inbox" />
            //   </ListItemButton>
            // </ListItem>
            <IconButton aria-label="settings">
              {/* <MoreVertIcon> */}
              {/* <nav aria-label="main mailbox folders"> */}
              {/* <Menu/> */}
              <Viewmore />
              {/* <Overlay/> */}

              {/* </nav> */}
              {/* <nav aria-label="main mailbox folders">
          <ListItem disablePadding>
        <ListItemButton>
          <ListItemIcon>
          <InboxIcon />
         </ListItemIcon>
          <ListItemText primary="Inbox" />
        </ListItemButton>
      </ListItem>
        </nav> */}
              {/* </MoreVertIcon> */}
            </IconButton>
          }
          title="Pani Puri"
          subheader="October 27, 2021"
        />
        <CardMedia
          component="img"
          height="120"
          width="120"
          // image="/static/images/cards/paella.jpg"
          image="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBQUFBcUFBUYGBcZFxgaGRoaGRkaGhkYGhkZGRkdGRgaICwjGh4pHhkaJDYkKS0vMzMzGiI4PjgyPSwyMy8BCwsLDw4PHRISHTIpIykvMjQ+NDMyNDI6OjoyMjIyMjIvNDIyNDQ0OjQ0MjIyMjIyNDIyMjIyMjIyMjIyMjIyMv/AABEIALcBEwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAAIDBAYBBwj/xABAEAACAQIEBAMGBQIFBAEFAQABAhEAAwQSITEFQVFhInGBBhMykaGxQsHR4fAjUgcUYnKCFTOi8ZJTg7LC4hb/xAAaAQACAwEBAAAAAAAAAAAAAAACAwEEBQAG/8QALREAAgIBBAICAQIFBQAAAAAAAQIAEQMEEiExQVETInEFYRUygZHwFKGxwdH/2gAMAwEAAhEDEQA/APMppTTZrk0y5QqPUEkAbmtDwzh4+W5/IUH4b8ZPRSR9B9ia0PDi2QkwRmMRuIjf+c6oanILonibWgwhcfyVyTX4hfBwNBoO29abEcPBsZ2aDA0J15/pzrIe8KBmYGFE6dzAobxD2husuSSigaSCJnmCYB9T5V2LIrjiNy8dmP4hiAsiedZ3F8RqljsW5PxAjt+9VGQxO/pTdnuKOX1CNu6W51aRKpcLwb3DCldNyxgDSd47Vbt5l0Ig/eN4OxpGRa6lrCbAJhKzfJGVtREa66dPKh/EMP7thHwtqO3UVbw6kmP5507jwi0J3zqBz1huflOvah07FXrwZ2vxK+Kz2IGzUpqAPSz1o7p57ZLANdDVX95SF2p3SNhloGlUKvUgauuAVqOrgFdqaxYZ2CLuf5J7VxYAWZKqWIUdmVyK5lrccM4JaTdQ5jUsJ+S7CpMdhLGQ5radoUAz2jas3+JoXoAke5sfwfIEskA+pgTXIrVYLBYYNkdCSdiSdeo7GiF/2UtXFJtyjRI1JUnuNfpTl1qFqoxD/p2RVuxMKRTYona4Pda41sLDIYYk+EevflWm4f7Fo3xux7iACeYAIJosusxYzRPPocwcGizZRYFD2ZhIppFej4r/AA4BE27xU9HXMD5FYI+RrGcZ4Lewlz3d5IJEqQZVx1Vuf0Pajx50yfymBk0+THywgoimkVOtssYUEnoBJ+QqS9grqDM1t1XqVIHzIphYA1cWFYi6lIiuEVKVphFTOBjCK4RTyK4RXQgYyKVOilXTrlkXK7mqvNWcHZznXYb9+1LL0LMJdOXalhvguFXL7wnUyAO0kEn1H85E71xlICqsncACYHM5Y770NsXGUQpjpt+YqfD4gi4rOSVJyuDr4TI0nbes3IC7ljNdcLY8dDxNHi0UBC6h18GhGYTB5H8MjnzobxXgXvVzWrgQAHwMSynTSCRmTQHfNuOUmrONu5LRtyCBHiP4cp0nkTJHLWOhobh+LuCQApB2JJB23JkyO2/el4VyKLUyhTvzMbiMKyMwfwspIg75hyj89qsYFA05gzDQaxA76t0/KtRjD7xi0gSsQIgxO87/ALVTPCVkHXlGXTXkIHp9Kv8AzFloio9NOw5gE2srQpZTIA5gmdNQetXcKt11ykswmTLHIGAygk9RPKjOH4akrKzOsuSQO8fLYcqLLYsopzyzcguiwOeupn0ioOQkdRyYgp5P9pQwODI0ENpq3IeVUeMoLgCD4V1BHNtp7iPzo5euErl+Feg/PrQu5apScG47L9l2mZK/ZZDDeh5GmBq0mJwwdSCKzt60UYqatI+6ZeXDsP7Rpamk12K4aOJC1EGNTJe61DSiuBkFAZfS5Rj2cb+sI3ytHnWdw6szKq6liAB1JMD616PwXhCYcT8TkQzfko5D6mqut1CpiIPZEt/p+jZ8wYdAgwzgcExEkx3q5i/ZtbiwGZTy2I9ViT86GXMUy865hMZiHbw3CADEwD6CsLEwUbmno8qMxoQPd4HcS4Uu6ZYIIPxA8x0HX5VadGQAAsBy1NalUa8QLhzZdtAN4nUa/PSrz8JR1yMunQbjy77UfynI316ivqi0w5mJwJOZySSTlMkyTEjU89APlWt4RBjTUco+R+c/WhuK9mblt4VgUI0MGfIgcxprRTDYe5bGuoHSZ0/n3qCp32ZxdTjpTD9orzI/kUB9tuF2sTh8rGCrBkYbhpggcjKyI8jyqPF+1Nu0mhDPyAJPz/TehiXruJYNdJ/0JsFG/wDDT2zHGLHcqLp/kP26kPCuDIgCooAnlvM7k8zRocKkbaHfp0gzVnD8IBAEAQOQ/XvVxMM1v4ZPbcftXYt17m7k5WWtqzyD229nf8s63EWLb6QNlfeB2IkxyynsKyhFex/4hBXwdzNCkFGWSNSHAiDzgkR514+RW3p3Lpz4mFqVCvx5kJFcIqUimkU+IBkcUqfFKuk3Gqk6Ubw9rKoFDcIkt5UZjQVSzN4mzpEA+0SrU4sFhHX70/C2gSAefTlRLE5ViCocjUEwHHUE89Kos9GhNMJYswJibQyxmaeckkSNvL7ULe066j6GjDsJMj9PSqbqBrH89Ku48oIozMy4NptepHhrlwkT9qI23uToT1+WtVbLDp/O1W0v8wAPn+tSZCwlw/AXLjAAmSQBrH7VoOJcAGFUF3UuYhQQTsDJA1jWJ6g+ubsYhtIMfT7UXsBWTmzfzepAFQixB4g9mLb6dqguLVm50II/9x6ioSs0siNBuVmSgfGrEa1pAlDOO2fBPSuRqYQMqbkMy0Uop5FcirVzMqcrYcF9lQVV78ywBCA5YB2zEaz2ERQT2ew4fEW1bYEsR1ygtHzAr0uwmZoGhrO1+pbHSp2Zq/p2lRwXcWBxKa+z9tIKW1BG0KAw9d6hu3SNCYPyithgMMFPiiTpr8v55UK9p+F2495bYT+Iddto561nBXcbnM0flRTtUf2md96TzNHMBACgaiBt1O5rJf5gitHwe46qCwAHLrB6jl+9Bnx0sPGxs1NdwoBpIHPftOn61obOX96AcKuqFMRqQdPlUnFuO2rCksZaJCA+InltsO5pmnIVZn6hWd6Ev491LqvSSevRfrNZr2l42Lc201f007mPWhq4u/cfM7QTrAGo129Jq8vs4lzVy2c8wZ5d9KMPuJqEMYQAEzH28M925JgHdj+3U61reF4QsZk8ogxvpyoeeEXMLcZXOZHjI4EDwz4SJ0MHbtWp4EgCyddfpET26Ul7bIFPiWNypi3L5lrD4ZlGjkEbzJ+9XVJGhEa1Lhzzjn9djTMbcUKSTy0A3nl61bAAXuZzMWap5/7bcN/zV9VLtlQfAsAC425JM65cvlr1Mgv/APDIR4blz/xI5/6RO1egDAo/wNrzBGo5+uv3qe0u6k6r0+VOTM6jg8Rb4cbH7DmeFcV4Zcw9w27g7g8mHIj8xyqgRXov+KCIBZA+LNc06LCz9cteeEVq4mLICZk5lCOQIyKVOilTIq5Pgdz6fnRJmoXhjBothkzNHrVFhZm7hcBJcwlwLvT8VeB5D13Hl+1K4npVC4CKrtgINy2uosVOFu9RM3YV2aTjlRKKgsbEYrVOjVEq1Mi00GIK1LCa0Twz66yd+cDXrG9DrSUX4fhMxA1370YEEmduW9dBHkKctieX2j5RWr4rwrLaUyLYJiS6oqxroB4mMgaKCTmoM1wFYNtww3GXTaZMxuKllrucjX1BnutKDcafwkdq0TEHUVlfaN4BHWlBbMazUpmdApRXQK7ViZkt8IdlvIy7hue0bN9Ca31vHZfED/DtNY72d4eblyRoqjxN3MQB3j5VsGw6gRHrOtZWuZS4HkTa/T0b4yfBjL/GLjbGNfr1qpf4k8asT08+tQY2yy6q0j0mrnDOHoFDuMzMJAOwHLQ7nnrSrVV3GPVCWoR3B+FzDvzEqo6HYt1np/6raYTDgQqiNpPTageExCq4J0HXpWrwN9CAUI8S6c9Y5+VVvtlazDysMa7Vl63gg4ysJ5bfY/Lasxx32eWyy3UnISQVMkq+4MncH6Ed9NpZdRMnb1oH7WY1yqWbVs3GchmMhQiiI+LSSdh0B7TZKLtNTOTI/wAg9QRwiyD4tz9gSwP2ma02FJGw16bdPlz+VCMDadFBNsjsCDHcAb/tXX9pLCeEHO5JhVjcdTy86Ti+vcdl+3XMMcbwnvLLqQGbKcg28cHKQex/OstbS7ZRnujJt4gw3B6edFbPEblzxk5V2AEfQnc679qlxvCRiFVbjNpJGUnQntEH1miba5sQE3YxTSPB8XtsAM5YmNp3rl++1zcRrI/U9f3oecAMO3uy0+EZWjLOpGu8HT+TRuxZBA5mPn/JqF3MSp8QjtH2Egt2NJmqeKxgttBzFm1GUSSBp5aa1oUw4A1NB+I21NwAa5RqT/qj9BVmgBEXZnn/ALUcHxOKum6gUqqwqTDdTuMuYk9axDoVJBBBBIIOhBGhBHWa9/s4Ufz9+Ved/wCJPCFRreIQAZzkfuQJRvPKCP8AitaGmzkkKZm6rAKLiYCKVPilV+ZtyNN6L8OugGhNTWrkVTHc2FagRNHcaarXFqlbxkb61KcSp502gZwciROIqMGleeahR6SyR6ZLEsrVhFqsj1cstqKGody5hk151oOGsFKGAIbNyGp0MkDX8tOlZ5H1ohbukHSjBkEXPS8Hj7RT+oFJXUGC7LO+Vm1Wco1rOcZxqszFTzJ339aCJizETVXF3pFS78SMeOjcYlyJ15TWR9o8TNzL03o9ib0AnpWJv3S7FjzNDhWzI1L0KjxeFPDiqlKasbBKIael+z1kJZReozN5tqflt6Vp8Ng8wGkzXn3s5xiUFtviXb/UvbuNvlW/4LxgHQjbbp0rCy4WGZi03sedTiAX1GcR9mTka4mkCcu4POB6UEw2KVkAnVfCe0DT6RWh9pvau3btZVILkRlG3PfvXm2DuuGLDdjqOs9vWi+EOtyEysO5qL6H4g4q1wvjHu5lTy2jcbaHlVXCcLuOJbwnfLvHnXMRwtkkZgD0YEfakWoNXHFGPJmls+0jMPCuQf3GJJ5wuw9SaLYFS4zEkk6kmZP8/wDW1ZnB4DMFAOyide1azhrrpyM66adfIDWhQ7mici7BCmEw8iW01kdTWU4tgkTGXGOmfKT28IzH1IrYPihBbQADUkwABufKvM+Je0K3cS7CGtyFVpOsAAnTkTJBnmKflTchCyvgYh7abHA2idD4V1jYzJ6/KjVshRAjt1rJ8I4sreFXkchIka7EiijcRUc5PQbx+XrSsZCRmRWcxcU4ab7hw+UhcsESp1J205nlWZx/tA2Eu+5ZHuMdFKEGT08UQfnWmHE8qtCEtBIBKwW6TOnIVhgrvduPcBFySIO6zBMdogDsO9Haqd9SFViNs0uE4tcuaHwdSCGYesRPzq9hcMVbNqQQdTuT36/z1DcIEEaevTlWpS1Kxv37xy786jGSe5GTjqJnGwidY1/n8isP/iji191atg6tckD/AEqCPuwojd9pbSrIfNvGXUab6jwwCK8143xJ8TdNxttlHReXrWnpMTFtx8TN1mVVTb5MF0qdFdrVqZFyCu0q7WfNeczU03KfFRXEIow06o4XeVSo9Us1SK5riLjENQij1asvQtGqzaelGOBhS3c1q0uI70KRjUweNz9aiMuERiKa+I9Kof5lQJBny1HzFDsViLtwHIPCDqSQDOvImeVEFJgtkCiM4vj80ov/ACP5UGq0cG5GYgb9RNNOEcR4d+4J+VWVCqKEoOWc2RK9KrVjh15xNu1ccdVRmGncCoXtMpKsCCDBBEEHuDqKKxFkETlp4INGrfEboGjkDbl9yKBVbwV0jSgyICLIjsWQqalxHYuC5JPc1sfZqwCDcI1nKvbTxEfOPQ1nsNatnVhHWDMd4Hb/ANGIJjg+K90WQnwMcyN3jYxoJAGu3zrN1VshCzV03DBmm74aBofTy7/StIuDS4uV1DKRzH58qxXC8eMrz8Qg8u+wnz+da3hWIPugSQSWMzp2Ig/P1qjgShREZqXLGwZUf2XCmbVxk10BGYfkfrQDBcYecrICczLIbQgGAZI2P51ovafjq4awxDRccFbY3MkQT5KDPy61mOHsjAFQPCAViNgQI13P60eRQlUIvGzNe6Gb6NeXxk+75qvOCPi/u5aHTtpUD8EsXAQ1sRGhAhhsRDAaGPT0olgF8OvnHTaiAtFmEDw6evepUGCWrieT3eGG1fNt9chmdpGhB9ZFa/giiBpuPzP6VX9urQt4q3ciFe3BPdTB+61zgWPWQpMEaKORGv1qMlk8wlYbeJrrGFUgSOnrPeqXHuGrkzj4l+qk6j03+fWilpwBIgjT8qq+0uOS3hbtxoj3bhZPxMy5UHqSKn4wy7YveQ1wFwpgLgnpz7mN6vcd4gbVi485ZBRdYMtoT2gSf+NZHgvGXLCLTEEfEPF4hpCrAkkmd+VRe0GOuPIuSiLHhMz01P8Ad2ERpNOwadrFwc2VaJgPiFwBekrlRRyXaY5CNAP4AxFWL7l2LHn9OgHYVERW7jTatTzmfN8j34kUUqfFKmRNyuVrlWnAqForOK1Ndcl+JHXGNONNNRGSkza13KeRB+/yNK8INRRVkAEXEhyDRknvWFSJiWnSrGCwksrODk3iYLD8h/O9SuFBgDXpHWlsVuqllFYi7qRJiG5k/wDH8yf2ojwXhtzEuUTYQWZpIQbanqdYHOPMiK3gROVoDHVW3RhE6fmK3nssq2sLBAzs7M0bE6KN+UKPnVXUZxiQkdy1hwF3APUoWuDYeyMj57kHxa5RP+kDU+p5etF19n8BeBCKQdpVmmfUkcu9R3OHtcec4CmTO8TrBG5P6VoOAcLW1JDli2XlEROw161nrkyNzZltlQcUJ557U+yr4MJcD57bkgEgBlbUw0aGYMERsdBzIcF4CtqyMRdRXuMAbaOJVFJXKWU7sZnXbTvW/wDa+xaOCue+iFUFZMf1ARkj/lA8iaEYS8LjLG0RHLbaKPPqcgVQPJ5MVgwIxLeoGXiNwMCtwlhACr8PYZBofLvWt9ofZi1jbWS6sMslLijxIecf3L1U7+cEVeEeza27wuhyVVmIXKAeeUTOsSPUVqDikGhZQ0cyKjGdn2BkZju4q584cT4I9l3tuRntuVYdY2KnmCIImNDQ422RgGBB79DXsXtRwdcVee6mYbDMFLA5Rl2GsaDUVhuJ8KKNlvKSBMQdx1Vuf8mtLHqQ3BlJ9MRyIFs38p8RMdj9uQ/gnnRDD5zrbbNEnTMCOcxGnz/WhuJwrIAT8JJCt156gbH796Yp0giY/n8/KmMgYWIC5ChozR4fGOplWM80bY/7Ggx/tPoTRbDe017Jlt3AupmVEg68y2UeRg6bVj8HilTcSOgMfPT061MbiFgyk2z1zb+eUDTyoDiXyIwZW9wrjGuu5e42djuSdY5aGIHlpVjh3EHtn8QnYxI6x5VWwuNWMtwAgbEkZfR129AD1NTOyAZrTNMiVJkEc/GDMebMT0G4U+BW4MJcpHIm34T7QqQQxUHeJA2H7UaX2ntJ8RAidjvtoBv8uted4FEuELcKIeUwrHWNCR7uPMz+ZHE8N92My3kc5fgyyO/9QOAdeiUAwfvCOQHsSv7VcYuYy4MqOtpB4ZRgSx+JiCJHKB271Q4ebwdQLTtrocjfciKOYXE2QMt1cp0gkuB3j3IJ+cU93tk/0mLE7KFDa9f6iEgT0BPY1xxqe5Ic+IYwD3FANwZexYeWk6H51S4oBdfxsl0AjIhIS0p1GYkM/vH1gToOQG5lTDO1oTctoxIkvat2+siZUbx8uUwI7eDFtg4bMyf6JAMGWEaAd2ZRE6kEgkuNVPAglyZTxGFuTluQgBjLqGaA2gUCYgxJG20xWd49e1CCNNTHXUD6E/PltR/imJVFJWSRpvpmMESo0YjeGOkTlOhGNuxJiY5TqY7nmat6dLa/Uz9dl2ptHmQEU0ipSKYauzHBjIrtdilXSblvG8N/FbO/4T07Nz9aEOpBhhB71Zm/a5Zx1Gv0ph4xOjp6ETWUof8AIm8Co7FSClT2x1r+wfUUz/O2h+CfU/rR03ozrX3GvYLAmNBudh86bgcOGYlvhWfUxoPLmf3pmJxrPoBpyA2HpV5ZVFTTTX1ME/LQf8aZbKvPmciqzX6l7DWM5EyQIH5AD7URu8IRsr25JOmdJm24/C6Hccp76wDNc4KjPkIU/wB0gNpB0YECNCAIOmtaLEYS28HDge80965MZ2ERv6jQDf1FR81cDuX8eInkwJZtFYGXxGCdGCzIBgwQG1+Hn6k0Tw821yEMNYlwQSdjyE8ttKu8AXMbiMmR1IJkhTl2BR1nSRuOu/QjhrVy/K4hfAoKI8iVYCCZOuc5QZjUk+VU3UP9T3LYfYLHUD/9Wy7DWpsPxS7cac5VR/bp6afzSqmL4IzW2a27MwIAhdTJj4QMwMaxrtUvDsG2GGS6QzkBm7A6AEHnAOnegdCi3IQ7yYm4tde49sXCAhjxMeQJMSf5NHOEMtx0CvmJUhxmLAGCdCefP+aCrPAbdyXYvmZtYYaDoJB+s1rPZ/glvDg5ASzRmZjJMfQR2Ao9it1EK7otN3A/tV7+1kKOxtuwSBAIc6gTvBjfrQvEZrSIwBJY6uwMGOhO+voNOZrRf4g+HChhoRdQz0MMPvWU96w1cHxBSsk7Geuw8qDIgseoa5yuMkCzD3CsZdAVoDoQSYXKUCn4pO/81o3xXhaX7LArmEAj01BGmh6H8poPgOFqAAgKhyrXWNwyx3NsBRBUTuepid61zkKjM58IBJJ2CxJmaNVHiA+Q8GeJcV4cti4yOC6HUcgynYjofsRWUvW2ttlbYiQeoMwfuPQ16J7RXUewCIm26hDyKsDPqdCdz4fOshxHDG6gIIzKYA01UzO2uhA/+RrS07saBEp6lVALA8DmBHEH+fT9qtYJQzQSAOraL9VK/aq96w6bqR9R86s8JZ88o0NrpLqSOxTfyPyNWitCU0cHqX8VZRVBQq3UoU09UM/+MeVWeG4JLpHjbzzK23Zbg+wqjjsbmhRmkHYnUeUW1f70T4TfRwc8OY2f3Dnl/wDVCnlzpLAgR4PMO/8AT7aJ4y1wDXxK0/W4xHnGlcwFm27BAhBLfCLtk+Hb4GWY5fCTrUGKdUQBbar5JYOvb3BLA9xNWODFlksbsbQbt2I8vdZ+mmYbb0uuI2+YXxNm0i+7hA8bvdupHlCIJ/8Atkab9bPA8IUtlnAg7eNnDLz1K21edoAuDtQ7/MFrirbzKpIEBroY6j8DK5J3/F+1zivF7Sk27b+8u/C3u3ts45HMzsyqN5BffdRsYAJ8SCanHuN71mFt8+UKLjraUlSNlYqbsDomUAx8OpEfFcbbRBb94C2kwtxxmPinks6cgx28QGtUruMt4dMifGZY+G2ACTqItIgZu5Ef6jqKz2LxjXDr69T5/pTUxMx/aVs2qTGO+fUm4pxBrhyyYGnL7DQen1M0Mp1cIq8iBBQmLlzNlazGEU2Kea4RUwAYylXa7XSY/GYkLGVWCiZG+QjcE/n+lVjiEYeIAjuAakG+o0Bg6deURvvU+Ft2rguKytbgEoAQwaBsQdtp0PXnvlUvc3FZgOYNfDWm20PrHyphwQGwB9afbwAIJzHSIIMjnzI8vnUF7DOh+PrsQduvSmA+A0MjyROm3HKPSrGJX+o24GZoncCTAPeqi5/7zRJnNwBy2ZtA20yogSB1AGvnUmNxEDia3gWLt27Y8AVgDqdQefLYfnvQ5eNTi8xORWZp2glgdCd9SftUGEOaVkamYJywpgELpMqSdCeY1E0v+norLcDvAJKjQHYjUxOs7R1qn9QSGl6mNFeoZuYsWouW7gRzoROVWBI2iIOgPkDpU+K48bloW+r53YDQ5dfFOvxQZ7VnMSRAMfhkdAecd/Kq+DcM6ozAZmA9CQPtSzjDciSHIO0z0PhfGURMyIWuH8bES2kaE8hpy5fKtxbFs18G+pVGtwGClQGUzzktAnnymquHujOMo0EQOWn8+lH8FaJue895sAoUrIEDrOk5d4pKOxBFceoWZFPnn3/5G4e3FgEZFcv4H18SnMwkbjksGSNDyAodhfay6PDkEjQk7yDV+xhrt0XBet/GSVCvFtMuq5cu2/xbx8qAcYwS28dmJJzW1aPwl9VZyOpyz5kmjaqJHFCRjFsA3MJ8T4o+I92txRlR1uQM3iZSCJ1iB0OlaDiOBW7aS4HVGX8ZgiD16mYOvfrWUsENM9wfKNZjXnHeivB/egZGdvdkghSAxJBnfkNAeexpOJmo7uYedF6XiWOEWHt5s9xXyMcwkkZQSM0kSJg7aUzj/FheU2R/2jlFwsjZXVgYVXkAa5ddeQ50awuACEtA8W/OYkAEdNT86xnulXEjCZMltC3uwhIDBvFJCayJO53ntT8RUWT4lVl6Agn2ltZbeYqBORVAJKqqtpCwAJka9ugrNWbgAYxy/MUd9qpVvdkt4WK+IAO0RLEdNgP9prK8QvFUVQZJMwOSjr5k/wDia0MJsj8ypnU7CD5H/MnuY0CqF25bP4RPUafar/s7ZtM/9RlnkGUEHymt9a4Vh2APubR7+7Qn7U/LqNviVtPoN3INTA8I4RexbZbcsoOrXNVX/lvPYV6Jwn2Ks2lm6zXG/wBRIQeSzMeZNFhctYS1OVVAGgAAHoBWV4l7RFplzE/CkExPNtl05+LfUVXbIXl9MS4x3c0l5sPbEKEXyAH2oXieMWh+IfMVjMTxHPsvXUszN6xCn0UVSe5O4HyFDUktNc/GrZ/GKaOI220MHvz+Y1rC4iwDqNKpi6ync0xcd9GLZ67noF3hS3PFabX+1jv5N+vzoTdtspKsCCNwapcG4uwYAtr961xVcVbjQXFHhP5HsfpRJqWxvsfr3K2f9NXKhyYu/XuZo1ynupBIIgjQjoabWjPPxpFNin1wiukgxkUq7SqIUajgL7tiSCwaZ1BAiOkanvRCziMJkCkNny6seT5vAViAR26HWg9m6sjNMRAjU/vV61gJ1Y5AJ0MMdfXTyP0NZJ47m+QW6lW5YXwgyF0A0337yOZ2qDFYUgAhpMhYIg+f0+lScSuwYVieUgROp+muw0ofeutJBPICfICmIp7nF/FRT3HzqzhL4UkH4WgNGp0Mggc4107+VDgJPUzVt4ECI66z9KMipyNDyGAGMhRlOp0YSDryCnb8Q5GKKf5NWZbwhlmDbDEGYJCvOimJ0PMdN8tYxGXQlskkhfxKY0YbRy2I3OvW9YxFy3F22zKJyhl0/wCJjQf7efSKVkxg9dy1jykcR/E8azOyqhBH4NJAHUChiZ1OeJIIYaajWQenL6VosPxq04C3kg5p94uYwDvpJKj/AGz2AGxPDcJt3dbV0OozA5yc4/tylRMeYB23NLFoOo3hzdyDBY0SjjYjTsdo9NRWwsuIB27xpJmJ+ZrJpwa7lL20EGRlcNbYsDB8LKJkDfT15W8G962mdyFTmrsSSQNgImdt6QMfoQnauzN3gydBOhA56da8/wDbfFquNLFv+2iCRquviIOuh1Hzo1wLi13EPkCBEyg5lZWbyZfwacjJ+VAeL8NUl1t3Cwe5mXNDPlY7sWgxMzqT66VKpbEGB8m2iJNwviCRmOxIEjUeo6Ed61vDMak6co5deg/m9Y7hnB7S23t3FPvS0Letm4pHPQMACYkQJBq4L9rDyrsVMAgPclwII2tsSdfwwN/Ku/09dTjmvua7ivFzbt5raM7bBRprEzPSOYmsR7Q8btvZttbdfeksbjqAGEwcqvuGnodNRzqjxHjTXV92mYJpuRnYDYFl2H+kepNZ108X9TwDkNj2Cr077DzgFmPALHuLfLQkz4hrrZ3GgEeQHIdWJJ8yaDYq3cd1lGGeBbXK2omAEkeLXpzmtz7LcGtXP6lwD3aEKqEx7y4dSWI1IAiRpMqNpFb3ApYC5Ut2lClYAt24zFs20aHN9SOdPORcRqpWLNk5M8BYFTBBBG4Mgg+R2rff4f4p3zB2LBSAJ7itPx72TwmJJJti27SfeWxlaSZl0+F9ZnYnqKxXs+r4LGPhrsAmACD4T+JGXsymfvrUPkXKhrsR2AMjgHo8S77W8TNy8yA+G2co8/xHzmR6Vmmei3tPhyl92/DcOdT1n4vUNPzHWhNowwb+0g/Ig0A6jGu4lFdyiimJxFu4pypDkiDoI6knmIFDrigHQyOsRPlQ2TJWoxlofjMPzojUF/ajRiDJdARBaLBraez2JMqZnUa9R+v6VkyK0vsfYZnJ/CpB9ef5VOoG5IemGxqEve0FvLeaNmCt6kQfqDQs1quMcO97DjRwIg7ESTHY6nWsxesspysCD0NXtPkVkAvkTzmv0z48rEjgm7kdNNOppqxKAnKVKlUXCkDcEdHNu4wGoBjXTQyJiNOtSXMKLTKiltSJZo0UnfQaCKdjUZ2e49yHYzB1bJmCBjBhRsBz0mI1pnEluDImYNmtqAByVQRv86zCSTyZteJC1r3kOWUIBvrqJ3AAOvbeqmLciVyZQDEESxPUk+W1EMHYe0pZlB1ULO4JPLp1nlBoZfvMWLENEnQ7kAQIkchHyo05PHUKyOKkNsDcT57b1MxBHOdeQqGziQCZ+HTTy/OnOGDGFIkyAeh1X59qIqb5hBqjQ52Ij+datYe/kZwrFRGsMeR2MHxDXy0qqobWf+RNNFo76ffv0qanE8cS7bxyAZXUmD8ax/8Aif1H51JZugjOHUQRuwVh5AmTy1H5VTRCdJHr07RVs8LkwAZiSQfoQRUELOGcr3DuG47eXLnyXMpzJ71A+UwRIY+KY5zVixx64CRcW3cDEsodQMk6QsaRE7gnvWQv4YpoLg+ZFNtYi78KsT5Enn15Ch+H0YwalWHIm4w3G/dsciKrMNYjQHWAFA085OtVrnFbn/bUALrKGXzzJhi5JI5RPOs9YS6ficiekZvIsdY02q3ZDwQC7c2Es3qRy37bipGA+5B1KDwYRv469cBzM2UaZRCoBGisF5bDXrvQ+5i7aLGZdI8IRtfIxln9+tMuMiauQOw1J08wBBj+A029fwrDKoaDOp1bWAJOUaQCYEfEOmvfEPJud87HoVKeI4oW+BAn+okk+fQH0ofzktJO+uteqeyPsdYS0mJxCC7cuqHto3wW0YSpKzDM0zroBHOtQ4tlSpt246e7SB6RtQNnRDtEE23JmM9gmFzDvbYksrlt91IC7nplHzFFeI8McqUAaDtlbaNRmmJA/Opr3AEtt73CD3biSUUQr9Qq8jA22O1PwXtCjgF1kETmQypB20/eqOVxuJ/3jBjDLUJ277G2nvZzhRnIj4oEkRoJivPv8SXjGWeTDDW83WfeXCJ7wR9K9DsY62P6g113Ow0J6b6bb9Ky3E+H2MXdL4plW6d8rkFQIg5iYK/h1AOaNxRaXlifFRrmlAHiDLGMt37YtXzE/A/MGOR5Hz0NCsfwe5a1jOnJ12juN1+3c1Xxdv3WW25ksrHZlAy3LlogZtdGtkelcw/Gb1jRWzJ0bUftTwjLxHHIrc/5/WQ1bs45lXKQGHQjQ+lTrxzCXP8Au2sjf3J//MT6zUq28E/w3yvYlfzWoKHyJB2t5ED3GEkxA6VSv3JrUf8AS8G2+IJ7BkH/AOtEcDwnBqZW21w9SCw/8oUfKjUVJ49zI8M4PdvkZVhObkaen9x8q2jX8NgVW0zqrRJXUt5tG096LqIGwQdAZPz5elAuPYyyBla2txjsCJ069qKi55gtkGNSQa/MtWeP4ZtBdT51bx+Ft3rJMiVBKMCDBiYnoawD4ND+BfmQfmKs4FjaR0UkB4kTIA6U5dOVYEcSjl12N0IYgzk1w0iaVXTMCcpUqVRc6MxmZrpY6KyAECPhjbbYEefh8qVvw+LLEazOvp56UqVZ+RRxNZHPMl4i7OhCqBB0PSdZA7jT1oHig5bXfcQdopUqLCBsghyX5kdy0CpbYgiRGmskEdNjpRXg5DZQ2gWQDqZzdR0nT19aVKuyH6y0f+pObRuXmUgDLBgagiGJ1MHUAfLvVpbar4pVIGnhlQYjkCY9DSpUh/H9ISfymQui3cK1wqFcMdQTHhIlhz1100oKvE3AKyD3Ig/SlSqwg7/MrjswcTNG+GD3nhUAHc8h3J/k0qVOEJupp8Jwi2gDvLSoLcgoh3lYPiYIka6eLbmLuPsZVIUKmUBioAhWK+8GXQiP6toAdtYGiqlSmJhgATB8QDB2VtwY37kfU61WyyD0/n6UqVTCnufCcUL+FsXRoDbUEdHUZCI6ZlInpU6YYAktrO3pvtXaVZ2QDcYwSxaWSOxH3rJrhrVtmYIqo7Mc0AOgLXQWBVSYypAUczNdpUeLqEZbLjUnT4hoJIBXO430i2iLI66TGte4hzArpcXNGklWMGJJA1a4oJHToWB7Sp6yGlDGcFTFOrYjKGhLQFvMMoRiXKk7kRqTqQSBJ8VYLiuPT/MXfdIFtZyESNAF8IMciYzGNJY0qVOXkcxZNVU5YxNo7oPlNFcO1rki/wDxpUqBoaMTCdnFW12AHktX14uFG1KlQr4jHYgGpSxfGnbRdKFXDmMnU9aVKr6IB1PP6jO7n7GMrlKlTIgTlcpUqgyYqVKlUTp//9k="
        />
        <CardContent>
          <Typography variant="body2" color="text.secondary">
            This impressive paella is a perfect party dish and a fun meal to
            cook together with your guests. Add 1 cup of frozen peas along with
            the mussels, if you like.
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <FormControlLabel
            control={
              <Checkbox
                icon={<FavoriteBorder />}
                checkedIcon={<Favorite />}
                name="checkedH"
              />
            }
          />
          {/* <IconButton > */}
          {/* style={{ color:"red" }}  */}
          {/* <FavoriteIcon />
      </IconButton> */}
          <IconButton aria-label="share">
            <ShareIcon />
            {/* <ShoppingCart
        fontSize="inherit"
        style={{ fontSize: "100px" }}
        /> */}
          </IconButton>

         
          <IconButton aria-label="add-to-cart" >
             <ShoppingCartIcon style={addtocart} onClick={(e)=>addToCart(e)}/>
          </IconButton>

          {/* <ExpandMore
        expand={expanded}
        onClick={handleExpandClick}
        aria-expanded={expanded}
        aria-label="show more"
      >
        <ExpandMoreIcon />
      </ExpandMore> */}
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Method:</Typography>
            <Typography paragraph>
              Heat 1/2 cup of the broth in a pot until simmering, add saffron
              and set aside for 10 minutes.
            </Typography>
            <Typography paragraph>
              Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet
              over medium-high heat. Add chicken, shrimp and chorizo, and cook,
              stirring occasionally until lightly browned, 6 to 8 minutes.
              Transfer shrimp to a large plate and set aside, leaving chicken
              and chorizo in the pan. Add pimentón, bay leaves, garlic,
              tomatoes, onion, salt and pepper, and cook, stirring often until
              thickened and fragrant, about 10 minutes. Add saffron broth and
              remaining 4 1/2 cups chicken broth; bring to a boil.
            </Typography>
            <Typography paragraph>
              Add rice and stir very gently to distribute. Top with artichokes
              and peppers, and cook without stirring, until most of the liquid
              is absorbed, 15 to 18 minutes. Reduce heat to medium-low, add
              reserved shrimp and mussels, tucking them down into the rice, and
              cook again without stirring, until mussels have opened and rice is
              just tender, 5 to 7 minutes more. (Discard any mussels that don’t
              open.)
            </Typography>
            <Typography>
              Set aside off of the heat to let rest for 10 minutes, and then
              serve.
            </Typography>
          </CardContent>
        </Collapse>
      </Card>
    </div>
  );
};
export default ReciepyviwerCard;
