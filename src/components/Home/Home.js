import ReciepyviwerCard from "../ReciepyviwerCard/ReciepyviwerCard";
import styles from "./Home.module.css";
const Home = () => {
  return (
    <div className={styles.maincontainer}>
      <div className={styles.heading}>
        <h1 className={styles.gradienttext}>Welcome to My artGallery!</h1>
      </div>
      <div className={styles.wrapper}>
        <ReciepyviwerCard />
        <ReciepyviwerCard />
        <ReciepyviwerCard />
        <ReciepyviwerCard />
        <ReciepyviwerCard />
        <ReciepyviwerCard />
      </div>
    </div>
    // <div className={styles.mainpage}>
    // <h1>welcome to My artGallery!</h1>
    // <ReciepyviwerCard/>
    // <ReciepyviwerCard/>
    // </div>
  );
};
export default Home;
